import {
  Button,
  BackLink,
  InsetText,
  Typography
} from 'govuk-ui';
import Link from "next/link";
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['birth-date-ineligible', 'common']);
  return (
    <>
      <BackLink href='/birth-date'/>
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('birth-date-ineligible:basedOnYourAnswer') }
      </Typography>
      <Typography>
        { t('birth-date-ineligible:youMustBeOver') }
      </Typography>
      <Typography variant='m'>
        { t('birth-date-ineligible:ifYouThinkYouAreOver') }
      </Typography>
      <Typography>
        { t('birth-date-ineligible:youMayHaveEntered') } <Link className="govuk-link govuk-link--no-visited-state" href="birth-date">{ t('birth-date-ineligible:enterYourDateOfBirth') }</Link> { t('birth-date-ineligible:again') }
      </Typography>
      <Typography variant='m'>
        { t('birth-date-ineligible:ifYouAreUnder') }
      </Typography>
      <Typography>
        { t('birth-date-ineligible:youMayBeEligibleFor') }
      </Typography>
      <InsetText>
        { t('birth-date-ineligible:liveInScotland') } <Link className="govuk-link govuk-link--no-visited-state" href="https://www.mygov.scot/adult-disability-payment">{ t('birth-date-ineligible:applyForADP') }</Link> { t('birth-date-ineligible:whichHasReplacedPIP') }
      </InsetText>
      <Link href="https://www.gov.uk/pip">
        <Button>
          { t('birth-date-ineligible:applyForPIP') }
        </Button>
      </Link>
      <Typography variant='m'>
        { t('birth-date-ineligible:ifYouStillWantToApply') }
      </Typography>
      <Typography>
        { t('birth-date-ineligible:youCanStill') } <Link className="govuk-link govuk-link--no-visited-state" href="other-benefits">{ t('birth-date-ineligible:applyForAttendanceAllowance') }</Link> { t('birth-date-ineligible:butBasedOnWhat') }
      </Typography>
    </>
  );
}
