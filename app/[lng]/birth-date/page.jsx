import {
  Button,
  BackLink,
  Hint,
  DateInput,
  Typography, Legend
} from 'govuk-ui';
import getPageData from "../../helpers/get-page-data";
import { useTranslation } from '../../i18n';
import renderErrorSummary from '../../helpers/error-summary';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('birth-date');
  const { t } = await useTranslation(lng, ['birth-date', 'common']);
  return (
    <>
      <BackLink href='/person-claiming'/>
      { validation && renderErrorSummary(validation) }
      <DateInput id="birth-date" name="birth-date" value={data} errorMessage={validation?.['birth-date-day']?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('birth-date:pageTitle') }
          </Typography>
        </Legend>
        <Hint>
          { t('birth-date:field.dateOfBirth.hint') }
        </Hint>
      </DateInput>
      <Button classes="govuk-body">
        { t('form.buttons.continue.label', { ns: 'common' }) }
      </Button>
    </>
  );
}
