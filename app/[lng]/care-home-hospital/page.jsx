import {
  Button,
  Radios,
  RadioItem,
  Hint,
  Label,
  BackLink,
  Typography, Legend
} from 'govuk-ui';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('care-home-hospital');
  const { t } = await useTranslation(lng, ['care-home-hospital', 'common']);
  return (
    <>
      <BackLink href='/home-address-select'/>
      { validation && renderErrorSummary(validation) }
      <Radios name="care-home-hospital" value={data?.["care-home-hospital"]} errorMessage={validation?.["care-home-hospital"]?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('care-home-hospital:pageTitle') }
          </Typography>
          <Hint>
            { t('care-home-hospital:field.careHomeOrHospital.hint.similarInstitutions') }
          </Hint>
        </Legend>
        <RadioItem value="yes">
          <Label>
            { t('care-home-hospital:field.careHomeOrHospital.options.yes') }
          </Label>
        </RadioItem>
        <RadioItem value="no">
          <Label>
            { t('care-home-hospital:field.careHomeOrHospital.options.no') }
          </Label>
        </RadioItem>
      </Radios>
      <Button classes="govuk-body">
        { t('common:form.buttons.continue.label') }
      </Button>
    </>
  );
}
