import {
  BackLink,
  SummaryList,
  SummaryListRow,
  Typography
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import { cookies } from 'next/headers';
import { getFormData } from "../../../lib/session";
import formatDate from "../../helpers/format-date";

const formData = async () => {
  const nextCookies = cookies();
  const sessionId = nextCookies.get(process.env.SESSION_COOKIE_NAME)?.value;
  return await getFormData(sessionId);
}

export default async function Index({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['check-your-answers-details', 'common']);
  const data = await formData();
  return (
    <>
      <BackLink href='/treatment-surgery-information' />
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-9">
        { t('check-your-answers-details:pageTitle') }
      </Typography>
      <Typography variant="m" component="h2" classes="govuk-!-margin-bottom-3">
        { t('check-your-answers-details:yourDetails') }
      </Typography>

      <SummaryList classes="govuk-!-margin-bottom-9">
        <SummaryListRow rowKey="Name" field="full-name" data={data['full-name']} href="full-name" />
        <SummaryListRow rowKey="Date of birth" field="date" data={{date: formatDate(`${data['birth-date']['birth-date-day']}-${data['birth-date']['birth-date-month']}-${data['birth-date']['birth-date-year']}`)}} href="birth-date" />
        <SummaryListRow rowKey="Your NI number" field="national-insurance" data={data['national-insurance']} href="national-insurance" />
        <SummaryListRow rowKey="Home address" field="home-address" data={data['home-address-select']} href="home-address-select" />
      </SummaryList>

      <Typography variant="m" component="h2" classes="govuk-!-margin-bottom-3">
        { t('check-your-answers-details:yourClaimInformation') }
      </Typography>

      <SummaryList classes="govuk-!-margin-bottom-9">
        <SummaryListRow rowKey="Who is is claiming" field="person-claiming" data={data['person-claiming']} href="person-claiming" />
        <SummaryListRow rowKey="Other benefits" field="other-benefits" data={data['other-benefits']} href="other-benefits" />
        <SummaryListRow rowKey="Nationality" field="nationality" data={data['nationality']} href="nationality" />
        <SummaryListRow rowKey="Where you live" field="residence-country" data={data['residence-country']} href="residence-country" />
        <SummaryListRow rowKey="Been outside of the UK more than 12 months in the last 3 years" field="country-outside-uk-duration" data={data['country-outside-uk-duration']} href="country-outside-uk-duration" />
        <SummaryListRow rowKey="Currently in care home or hospital" field="care-home-hospital" data={data['care-home-hospital']} href="care-home-hospital" />
      </SummaryList>

      <Typography variant="m" component="h2" classes="govuk-!-margin-bottom-3">
        { t('check-your-answers-details:yourHealthInformation') }
      </Typography>

      <SummaryList classes="govuk-!-margin-bottom-9">
        <SummaryListRow rowKey="Your illness or conditions" field="illness-disability" data={data['illness-disability']} href="illness-disability" />
        <SummaryListRow rowKey="Special rules" field="special-rules-declaration" data={data['special-rules-declaration']} href="special-rules-declaration" />
        <SummaryListRow rowKey="Renal dialysis treatment" field="renal-dialysis" data={data['renal-dialysis']} href="renal-dialysis" />
        <SummaryListRow rowKey="Other surgeries or treatments" field="treatment-surgery" data={data['treatment-surgery']} href="treatment-surgery" />
      </SummaryList>
    </>
  );
}
