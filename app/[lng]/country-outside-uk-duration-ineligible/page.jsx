import { BackLink, Typography } from 'govuk-ui';
import { useTranslation } from '../../i18n';
import Link from 'next/link';

export default async function Index({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['country-outside-uk-duration-ineligible', 'common']);
  return (
    <>
      <BackLink href='/country-outside-uk-duration'/>
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('country-outside-uk-duration-ineligible:basedOnYourAnswers') }
      </Typography>
      <Typography>
        { t('country-outside-uk-duration-ineligible:thereAreSomeCircumstances') } <Link href="https://www.gov.uk/government/publications/attendance-allowance-claim-form" className="govuk-link">{ t('country-outside-uk-duration-ineligible:claimBenefits') }</Link>.
      </Typography>
      <Typography variant='m'>
        { t('country-outside-uk-duration-ineligible:ifYouStillWantToApply') }
      </Typography>
      <Typography>
        { t('country-outside-uk-duration-ineligible:youCan') } <Link href="national-insurance" className="govuk-link">{ t('country-outside-uk-duration-ineligible:applyForAttendanceAllowance') }</Link> { t('country-outside-uk-duration-ineligible:butYouMayNot') }
      </Typography>
    </>
  );
}
