import { useTranslation } from "../../i18n";

export default async function Head({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['country-outside-uk-duration', 'common']);
  return (
    <>
      <title>{`${ t('country-outside-uk-duration:pageTitle') } - ${ t('common:serviceName') } - ${ t('common:govuk') }`}</title>
    </>
  );
}
