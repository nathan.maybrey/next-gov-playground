import {
  Button,
  BackLink,
  Radios,
  RadioItem,
  Label,
  Typography, Legend
} from 'govuk-ui';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('country-outside-uk-duration');
  const { t } = await useTranslation(lng, ['country-outside-uk-duration', 'common']);
  return (
    <>
      <BackLink href='/residence-country'/>
      { validation && renderErrorSummary(validation) }
      <Radios name="country-outside-uk-duration" value={data?.["country-outside-uk-duration"]} errorMessage={validation?.["country-outside-uk-duration"]?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('country-outside-uk-duration:pageTitle') }
          </Typography>
        </Legend>
        <RadioItem value="yes">
          <Label>
            { t('country-outside-uk-duration:field.outsideUKDuration.options.yes') }
          </Label>
        </RadioItem>
        <RadioItem value="no">
          <Label>
            { t('country-outside-uk-duration:field.outsideUKDuration.options.no') }
          </Label>
        </RadioItem>
      </Radios>
      <Button>
        { t('common:form.buttons.continue.label') }
      </Button>
    </>
  );
}
