import {
  Button,
  Input,
  Hint,
  BackLink,
  Typography, Label,
} from 'govuk-ui';
import renderErrorSummary from '../../helpers/error-summary';
import getPageData from "../../helpers/get-page-data";
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('full-name');
  const { t } = await useTranslation(lng, ['full-name', 'common']);
  return (
    <>
      <BackLink href='/national-insurance'/>
      { validation && renderErrorSummary(validation) }
      <Input name="full-name" id="full-name" value={data?.['full-name']} errorMessage={validation?.['full-name']?.inline}>
        <Label>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('full-name:pageTitle') }
          </Typography>
        </Label>
        <Hint>
          { t('full-name:field.fullName.hint') }
        </Hint>
      </Input>
      <Button>
        { t('common:form.buttons.continue.label') }
      </Button>
    </>
  );
}
