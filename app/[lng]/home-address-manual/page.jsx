import {
  Button,
  Input,
  Hint,
  BackLink,
  Typography, Label
} from 'govuk-ui';
import getPageData from "../../helpers/get-page-data";
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('home-address-manual');
  const { t } = await useTranslation(lng, ['home-address-manual', 'common']);
  return (
    <>
      <BackLink href='/home-address-postcode'/>
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('home-address-manual:pageTitle') }
      </Typography>
      <Hint>
        { t('home-address-manual:hint') }
      </Hint>
      <Input name="address1" id="address1" value={data?.["address1"]}>
        <Label>
          { t('home-address-manual:field.addressLine1.label') }
        </Label>
      </Input>
      <Input name="address2" id="address2" value={data?.["address2"]}>
        <Label>
          { t('home-address-manual:field.addressLine2.label') }
        </Label>
      </Input>
      <Input name="address3" id="address3" classes="govuk-input govuk-!-width-two-thirds" value={data?.["address3"]}>
        <Label>
          { t('home-address-manual:field.town.label') }
        </Label>
      </Input>
      <Input name="postcode" id="postcode" classes="govuk-input govuk-input--width-10" value={data?.["postcode"]}>
        <Label>
          { t('home-address-manual:field.postcode.label') }
        </Label>
      </Input>
      <Input name="address4" id="address4" classes="govuk-input govuk-!-width-two-thirds" value={data?.["address4"]}>
        <Label>
          { t('home-address-manual:field.country.label') }
        </Label>
      </Input>
      <Button>
        { t('common:form.buttons.continue.label') }
      </Button>
    </>
  );
}
