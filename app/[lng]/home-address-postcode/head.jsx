import { useTranslation } from "../../i18n";

export default async function Head({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['home-address-postcode', 'common']);
  return (
    <>
      <title>{`${ t('home-address-postcode:pageTitle') } - ${ t('common:serviceName') } - ${ t('common:govuk') }`}</title>
    </>
  );
}
