import {
  Button,
  Input,
  Label,
  BackLink,
  Typography,
} from 'govuk-ui';
import Link from "next/link";
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('home-address-postcode');
  const { t } = await useTranslation(lng, ['home-address-postcode', 'common']);
  return (
    <>
      <BackLink href='/national-insurance'/>
      { validation && renderErrorSummary(validation) }
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('home-address-postcode:pageTitle') }
      </Typography>
      <Typography>
        { t('home-address-postcode:field.homeAddressPostcode.hint') }
      </Typography>
      <Input name="postcode" id="postcode" classes="govuk-input--width-5" value={data?.postcode} errorMessage={validation?.postcode?.inline}>
        <Label>
          <Typography classes="govuk-label">
            { t('home-address-postcode:field.homeAddressPostcode.label') }
          </Typography>
        </Label>
      </Input>
      <Button>
        { t('home-address-postcode:field.homeAddressPostcode.button') }
      </Button>
      <Link href="home-address-manual" className="govuk-body govuk-link">
        <Typography>
          { t('home-address-postcode:manualLink') }
        </Typography>
      </Link>
    </>
  );
}
