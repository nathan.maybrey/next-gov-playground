import { useTranslation } from "../../i18n";

export default async function Head({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['home-address-select', 'common']);
  return (
    <>
      <title>{`${ t('home-address-select:pageTitle') } - ${ t('common:serviceName') } - ${ t('common:govuk') }`}</title>
    </>
  );
}
