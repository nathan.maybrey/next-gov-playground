import {
  Button,
  Hint,
  BackLink,
  Typography,
  Select,
} from 'govuk-ui';
import Link from "next/link";
import { redirect } from 'next/navigation';
import getPageData from "../../helpers/get-page-data";
import { getAddressesByPostcode } from '../../../lib/addresses';
import renderErrorSummary from '../../helpers/error-summary';
import { useTranslation } from '../../i18n';

const fetchAddresses = async () => {
  const { data } = await getPageData('home-address-postcode');
  try {
    const addresses = await getAddressesByPostcode(data?.postcode);
    if (!addresses) {
      redirect('/home-address-manual');
    }

    // Extract only the parts of addresses we need to avoid storing large
    // volumes of data in session
    const filteredAddresses = addresses.reduce((acc, address) => {
      if (address.singleLine && address.singleLine !== '') {
        acc.push({
          uprn: address.uprn,
          postcode: address.postcode,
          singleLine: address.singleLine,
        });
      }
      return acc;
    }, []);

    return {
      postcode: data.postcode,
      addresses: filteredAddresses
    };
  } catch (e) {
    console.log("Error fetching addresses, redirecting to manual entry");
    redirect('home-address-manual');
  }
}

export default async function Index({ params: { lng } }) {
  const { postcode, addresses } = await fetchAddresses();
  const { data, validation } = await getPageData('home-address-select');
  const { t } = await useTranslation(lng, ['home-address-select', 'common']);
  return (
    <>
      <BackLink href='/home-address-postcode'/>
      { validation && renderErrorSummary(validation) }
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('home-address-select:pageTitle') }
      </Typography>
      <Hint>
        { t('home-address-select:hint') }
      </Hint>
      <Typography variant="m" component="h2">
        { t('home-address-select:postcode') }
      </Typography>
      <Typography>
        {postcode} <Link href="home-address-postcode">{ t('home-address-select:field.homeAddressSelect.change') } <span className="govuk-visually-hidden">{ t('home-address-select:field.homeAddressSelect.yourHomePostcode') }</span></Link>
      </Typography>
      <Typography variant="m" component="h2">
        { t('home-address-select:field.homeAddressSelect.label') }
      </Typography>
      <Select name='home-address' id='home-address' errorMessage={validation?.['home-address']?.inline}>
        <option value='select-address'>{ t('home-address-select:field.homeAddressSelect.label') }</option>
        { addresses.map((address) => {
          return (
            <option key={address.uprn} value={address.uprn}>
              {address.singleLine}
            </option>
          )
        })}
      </Select>
      <Button>
        { t('common:form.buttons.continue.label') }
      </Button>
    </>
  );
}
