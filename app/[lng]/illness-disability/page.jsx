import {
  Button,
  BackLink,
  Typography,
  InsetText,
  Textarea
} from 'govuk-ui';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('illness-disability');
  const { t } = await useTranslation(lng, ['illness-disability', 'common']);
  return (
    <>
      <BackLink href='/national-insurance'/>
      { validation && renderErrorSummary(validation) }
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('illness-disability:pageTitle') }
      </Typography>
      <Typography>
        { t('illness-disability:field.illnessDisability.tellUs') }
      </Typography>
      <Typography>
        { t('illness-disability:field.illnessDisability.weNeedToKnow') }
      </Typography>
      <ul className="govuk-body">
        <li>
          { t('illness-disability:field.illnessDisability.nameOfYourIllness') }
        </li>
        <li>
          { t('illness-disability:field.illnessDisability.partOfTheBody') }
        </li>
        <li>
          { t('illness-disability:field.illnessDisability.tabletsOrMedication') }
        </li>
        <li>
          { t('illness-disability:field.illnessDisability.dosage') }
        </li>
      </ul>
      <Typography variant='m'>
        { t('illness-disability:field.illnessDisability.example') }
      </Typography>
      <InsetText>
        { t('illness-disability:field.illnessDisability.arthritisExample') }
        <br/>
        { t('illness-disability:field.illnessDisability.diabetesExample') }
      </InsetText>
      <Textarea name='illness-disability' id='illness-disability' rows={9} value={data?.['illness-disability']} errorMessage={validation?.['illness-disability']?.inline}/>
      <Button>
        { t('common:form.buttons.continue.label') }
      </Button>
    </>
  );
}
