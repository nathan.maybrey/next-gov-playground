import {
  Button,
  Input,
  Hint,
  BackLink,
  Typography, Label,
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import renderErrorSummary from '../../helpers/error-summary';
import getPageData from "../../helpers/get-page-data";

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('national-insurance');
  const { t } = await useTranslation(lng, ['national-insurance', 'common']);
  return (
    <>
      <BackLink href='/country-outside-uk-duration'/>
      { validation && renderErrorSummary(validation) }
      <Input name="national-insurance" value={data && data['national-insurance']} errorMessage={validation?.['national-insurance']?.inline}>
        <Label>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('national-insurance:pageTitle') }
          </Typography>
        </Label>
        <Hint>
          { t('national-insurance:field.nationalInsurance.hint') }
        </Hint>
      </Input>
      <Button>
        { t('common:form.buttons.continue.label') }
      </Button>
    </>
  );
}
