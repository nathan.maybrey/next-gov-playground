import {
  Button,
  Input,
  Details,
  DetailsSummary,
  BackLink,
  Label,
  Typography
} from 'govuk-ui';
import getPageData from "../../helpers/get-page-data";
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('nationality-other');
  const { t } = await useTranslation(lng, ['nationality-other', 'common']);
  return (
    <>
      <BackLink href='/nationality'/>
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('nationality-other:pageTitle') }
      </Typography>
      <Input name="nationality" id="nationality" value={data?.nationality}>
        <Label>
          { t('nationality-other:field.nationalityOther.hint') }
        </Label>
      </Input>
      <Details>
        <DetailsSummary>{ t('nationality-other:field.nationalityOther.details.nationalityOther.title') }</DetailsSummary>
        { t('nationality-other:field.nationalityOther.details.nationalityOther.content') }
      </Details>
      <Button>
        { t('common:form.buttons.continue.label') }
      </Button>
    </>
  );
}
