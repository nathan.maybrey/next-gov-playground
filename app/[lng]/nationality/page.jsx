import {
  Button,
  BackLink,
  Label,
  Radios,
  RadioItem,
  Details,
  DetailsSummary,
  Typography, Legend
} from 'govuk-ui';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from "../../helpers/error-summary";
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('nationality');
  const { t } = await useTranslation(lng, ['nationality', 'common']);
  return (
    <>
      <BackLink href='/other-benefits'/>
      { validation && renderErrorSummary(validation) }

      <Radios name="nationality" value={data?.nationality} errorMessage={validation?.nationality?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('nationality:pageTitle') }
          </Typography>
        </Legend>
        <RadioItem value='british'>
          <Label>
            { t('nationality:field.nationality.options.british') }
          </Label>
        </RadioItem>
        <RadioItem divider={true}>
          { t('common:or') }
        </RadioItem>
        <RadioItem value='other'>
          <Label>
            { t('nationality:field.nationality.options.other') }
          </Label>
        </RadioItem>
      </Radios>

      <Button>
        { t('common:form.buttons.continue.label') }
      </Button>
      <Details>
        <DetailsSummary>{ t('nationality:field.nationality.details.nationality.title') }</DetailsSummary>
        { t('nationality:field.nationality.details.nationality.content') }
      </Details>
    </>
  );
}
