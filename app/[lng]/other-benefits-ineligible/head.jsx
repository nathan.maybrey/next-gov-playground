import { useTranslation } from "../../i18n";

export default async function Head({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['other-benefits-ineligible', 'common']);
  return (
    <>
      <title>{`${ t('other-benefits-ineligible:pageTitle') } - ${ t('common:serviceName') } - ${ t('common:govuk') }`}</title>
    </>
  );
}
