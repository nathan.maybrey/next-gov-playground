import {
  BackLink,
  InsetText,
  Typography
} from 'govuk-ui';
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['other-benefits-ineligible', 'common']);
  return (
    <>
      <BackLink href='/other-benefits'/>
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('basedOnYourAnswers') }
      </Typography>
      <Typography>
        { t('youAreNotEligible') }
      </Typography>
      <ul className="govuk-list govuk-list--bullet">
        <li>{ t('disabilityLivingAllowance') }</li>
        <li>{ t('personalIndependancePayment') }</li>
        <li>{ t('adultDisabilityPayment') }</li>
      </ul>
      <Typography variant="m">
        { t('ifYouStillWantToApply') }
      </Typography>
      <Typography>
        { t('youCanStill') } <a className="govuk-link" href="national-insurance">{ t('applyForAttendanceAllowance') }</a> { t('butYouMayNotBe') }
      </Typography>
      <InsetText>
        { t('ifYouAreRecievingPIP') }
      </InsetText>
    </>
  );
}
