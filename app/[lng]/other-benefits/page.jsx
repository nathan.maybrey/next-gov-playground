import {
  Button,
  BackLink,
  Hint,
  Checkboxes,
  CheckboxItem,
  Label,
  Typography
} from 'govuk-ui';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('other-benefits');
  const { t } = await useTranslation(lng, ['other-benefits', 'common']);
  return (
    <>
      <BackLink href='/birth-date'/>
      { validation && renderErrorSummary(validation) }
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('other-benefits:pageTitle') }
      </Typography>
      <Hint>
        { t('other-benefits:field.otherBenefits.hint') }
      </Hint>
      <Checkboxes name="other-benefits" value={data?.['other-benefits']} errorMessage={validation?.['other-benefits']?.inline}>
        <CheckboxItem name="dla" value="dla">
          <Label>{ t('other-benefits:field.otherBenefits.options.dla') }</Label>
        </CheckboxItem>
        <CheckboxItem name="pip" value="pip">
          <Label>{ t('other-benefits:field.otherBenefits.options.pip') }</Label>
        </CheckboxItem>
        <CheckboxItem name="adp" value="adp">
          <Label>{ t('other-benefits:field.otherBenefits.options.adp') }</Label>
        </CheckboxItem>
        <CheckboxItem name="caa" value="caa">
          <Label>{ t('other-benefits:field.otherBenefits.options.caa') }</Label>
        </CheckboxItem>
        <CheckboxItem name="divider" divider={true}>
          { t('common:or') }
        </CheckboxItem>
        <CheckboxItem name="none" value="none" exclusive={true}>
          <Label>{ t('other-benefits:field.otherBenefits.options.none') }</Label>
        </CheckboxItem>
      </Checkboxes>
      <Button>
        { t('common:form.buttons.continue.label') }
      </Button>
    </>
  );
}
