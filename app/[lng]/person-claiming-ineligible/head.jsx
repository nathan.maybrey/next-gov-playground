import { useTranslation } from "../../i18n";

export default async function Head({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['person-claiming-ineligible', 'common']);
  return (
    <>
      <title>{`${ t('person-claiming-ineligible:pageTitle') } - ${ t('common:serviceName') } - ${ t('common:govuk') }`}</title>
    </>
  );
}
