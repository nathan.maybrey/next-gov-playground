import {
  BackLink,
  Typography
} from 'govuk-ui';
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['person-claiming-ineligible', 'common']);
  return (
    <>
      <BackLink href='/person-claiming'/>
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('basedOnYourAnswers') }
      </Typography>
      <Typography>
        { t('youWillNeedToUse') } <a href="https://www.gov.uk/government/publications/attendance-allowance-claim-form" className="govuk-link">{ t('paperClaimForm') }</a> { t('ifYouAreApplying') }
      </Typography>
      <ul className="govuk-list govuk-list--bullet">
        <li>{ t('lastingPowerOfAttorney') }</li>
        <li>{ t('someoneMeetsSpecialRules') }</li>
        <li>{ t('yourCondition') }</li>
      </ul>
    </>
  );
}
