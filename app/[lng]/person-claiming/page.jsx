import {
  Button,
  BackLink,
  Radios,
  RadioItem,
  Label,
  Hint,
  Typography,
  Legend
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from "../../helpers/error-summary";

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('person-claiming');
  const { t } = await useTranslation(lng, ['person-claiming', 'common']);
  return (
    <>
      <BackLink href='/start'/>
      { validation && renderErrorSummary(validation) }
      <Radios name="person-claiming" value={data?.['person-claiming']} errorMessage={validation?.['person-claiming']?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('pageTitle') }
          </Typography>
        </Legend>
        <RadioItem value="myself">
          <Label>
            { t('field.personClaiming.options.myself') }
          </Label>
          <Hint>
            { t('field.personClaiming.hints.myself') }
          </Hint>
        </RadioItem>
        <RadioItem value="official-capacity">
          <Label>
            { t('field.personClaiming.options.officialCapacity') }
          </Label>
          <Hint>
            { t('field.personClaiming.hints.officialCapacity') }
          </Hint>
        </RadioItem>
      </Radios>
      <Button classes="govuk-body govuk-!-margin-top-6">
        { t('form.buttons.continue.label', { ns: 'common' }) }
      </Button>
    </>
  );
}
