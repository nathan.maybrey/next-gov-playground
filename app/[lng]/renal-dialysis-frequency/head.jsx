import { useTranslation } from "../../i18n";

export default async function Head({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['renal-dialysis-frequency', 'common']);
  return (
    <>
      <title>{`${ t('renal-dialysis-frequency:pageTitle') } - ${ t('common:serviceName') } - ${ t('common:govuk') }`}</title>
    </>
  );
}
