import {
  Button,
  BackLink,
  Radios,
  RadioItem,
  Label,
  Typography, Legend
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';

export default async function Index({params: {lng}}) {
  const { t } = await useTranslation(lng, ['renal-dialysis-frequency', 'common']);
  const { data, validation } = await getPageData('renal-dialysis-frequency');
  return (
    <>
      <BackLink href='/renal-dialysis-when'/>
      { validation && renderErrorSummary(validation) }
      <Radios name="renal-dialysis-frequency" value={data?.['renal-dialysis-frequency']} errorMessage={validation?.['renal-dialysis-frequency']?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('pageTitle') }
          </Typography>
        </Legend>
        <RadioItem value="1">
          <Label>
            { t('field.renalDialysisFrequency.options.one') }
          </Label>
        </RadioItem>
        <RadioItem value="2">
          <Label>
            { t('field.renalDialysisFrequency.options.two') }
          </Label>
        </RadioItem>
        <RadioItem value="3+">
          <Label>
            { t('field.renalDialysisFrequency.options.threeOrMore') }
          </Label>
        </RadioItem>
      </Radios>
      <Button>
        { t('form.buttons.continue.label', {ns: 'common'}) }
      </Button>
    </>
  );
}
