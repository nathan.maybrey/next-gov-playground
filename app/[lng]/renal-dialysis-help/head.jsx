import { useTranslation } from "../../i18n";

export default async function Head({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['renal-dialysis-help', 'common']);
  return (
    <>
      <title>{`${ t('renal-dialysis-help:pageTitle') } - ${ t('common:serviceName') } - ${ t('common:govuk') }`}</title>
    </>
  );
}
