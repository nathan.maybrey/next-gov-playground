import {
  Button,
  BackLink,
  Radios,
  RadioItem,
  Label,
  Typography, Legend
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';

export default async function Index({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['renal-dialysis-help', 'common']);
  const { data, validation } = await getPageData('renal-dialysis-help');
  return (
    <>
      <BackLink href='/renal-dialysis-when' />
      { validation && renderErrorSummary(validation) }
      <Radios name="renal-dialysis-help" value={data?.['renal-dialysis-help']} errorMessage={validation?.['renal-dialysis-help']?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('pageTitle') }
          </Typography>
        </Legend>
        <RadioItem value="yes">
          <Label>
            { t('field.renalDialysisHelp.options.yes') }
          </Label>
        </RadioItem>
        <RadioItem value="no">
          <Label>
            { t('field.renalDialysisHelp.options.no') }
          </Label>
        </RadioItem>
      </Radios>
      <Button>
        { t('form.buttons.continue.label', { ns: 'common' }) }
      </Button>
    </>
  );
}
