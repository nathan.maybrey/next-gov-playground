import {
  Button,
  BackLink,
  Radios,
  RadioItem,
  Label,
  Typography, Legend
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';

export default async function Index({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['renal-dialysis-when', 'common']);
  const { data, validation } = await getPageData('renal-dialysis-when');
  return (
    <>
      <BackLink href='/renal-dialysis-where' />
      { validation && renderErrorSummary(validation) }
      <Radios name="renal-dialysis-when" value={data?.['renal-dialysis-when']} errorMessage={validation?.['renal-dialysis-when']?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('pageTitle') }
          </Typography>
        </Legend>
        <RadioItem value="day">
          <Label>
            { t('field.renalDialysisWhen.options.day') }
          </Label>
        </RadioItem>
        <RadioItem value="night">
          <Label>
            { t('field.renalDialysisWhen.options.night') }
          </Label>
        </RadioItem>
      </Radios>
      <Button>
        { t('form.buttons.continue.label', { ns: 'common' }) }
      </Button>
    </>
  );
}
