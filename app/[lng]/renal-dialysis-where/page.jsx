import {
  Button,
  BackLink,
  Radios,
  RadioItem,
  Label,
  Typography, Legend
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('renal-dialysis-where');
  const { t } = await useTranslation(lng, ['renal-dialysis-where', 'common']);
  return (
    <>
      <BackLink href='/renal-dialysis' />
      { validation && renderErrorSummary(validation) }
      <Radios name="renal-dialysis-where" value={data?.['renal-dialysis-where']} errorMessage={validation?.['renal-dialysis-where']?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('pageTitle') }
          </Typography>
        </Legend>
        <RadioItem value="home">
          <Label>
            { t('field.renalDialysisWhere.options.home') }
          </Label>
        </RadioItem>
        <RadioItem value="hospital">
          <Label>
            { t('field.renalDialysisWhere.options.hospital') }
          </Label>
        </RadioItem>
      </Radios>
      <Button>
        { t('form.buttons.continue.label', { ns: 'common' }) }
      </Button>
    </>
  );
}
