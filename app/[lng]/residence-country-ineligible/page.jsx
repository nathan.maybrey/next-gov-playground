import { 
  BackLink,
  Typography
} from 'govuk-ui';
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['residence-country-ineligible', 'common']);
  return (
    <>
      <BackLink href='/residence-country'/>
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('basedOnYourAnswer') }
      </Typography>
      <Typography>
        { t('thereAreSomeCircumstances') } <a href="https://www.gov.uk/claim-benefits-abroad/disability-benefits">{ t('claimBenefits') }</a>
      </Typography>
      <Typography variant="m">
        { t('ifYouStillWantToApply') }
      </Typography>
      <Typography>
        { t('ifYouStillWantToApply') } <a href="country-outside-uk-duration" class="govuk-link">{ t('applyForAttendanceAllowance') }</a> { t('butYouMayNot') }
      </Typography>
    </>
  );
}
