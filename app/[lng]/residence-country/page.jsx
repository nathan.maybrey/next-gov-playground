import {
  Button,
  BackLink,
  Radios,
  RadioItem,
  Label,
  Typography, Legend
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('residence-country');
  const { t } = await useTranslation(lng, ['residence-country', 'common']);
  return (
    <>
      <BackLink href='/nationality'/>
      { validation && renderErrorSummary(validation) }
      <Radios name="residence-country" value={data?.["residence-country"]} errorMessage={validation?.["residence-country"]?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('pageTitle') }
          </Typography>
        </Legend>
        <RadioItem value="england">
          <Label>
            { t('field.residenceCountry.options.england') }
          </Label>
        </RadioItem>
        <RadioItem value="scotland">
          <Label>
            { t('field.residenceCountry.options.scotland') }
          </Label>
        </RadioItem>
        <RadioItem value="wales">
          <Label>
            { t('field.residenceCountry.options.wales') }
          </Label>
        </RadioItem>
        <RadioItem value="northern-ireland">
          <Label>
            { t('field.residenceCountry.options.northernIreland') }
          </Label>
        </RadioItem>
        <RadioItem divider={true}>
          { t('common:or') }
        </RadioItem>
        <RadioItem value="somewhere-else">
          <Label>
            { t('field.residenceCountry.options.somewhereElse') }
          </Label>
        </RadioItem>
      </Radios>
      <Button>
        { t('form.buttons.continue.label', { ns: 'common' }) }
      </Button>
    </>
  );
}
