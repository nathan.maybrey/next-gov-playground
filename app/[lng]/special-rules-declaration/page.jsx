import {
  Button,
  Radios,
  RadioItem,
  Hint,
  Label,
  BackLink,
  Typography, Legend
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('special-rules-declaration');
  const { t } = await useTranslation(lng, ['special-rules-declaration', 'common']);
  return (
    <>
      <BackLink href='/illness-disability'/>
      { validation && renderErrorSummary(validation) }
      <Radios name="special-rules-declaration" value={data?.['special-rules-declaration']} errorMessage={validation?.['special-rules-declaration']?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classses="govuk-!-margin-bottom-3">
            { t('pageTitle') }
          </Typography>
          <Hint>
            { t('field.specialRulesDeclaration.hints.officialCapacity') }
          </Hint>
          <Hint>
            { t('field.specialRulesDeclaration.hints.specialRules') }
          </Hint>
        </Legend>
        <RadioItem value="yes">
          <Label>
            { t('field.specialRulesDeclaration.options.yes') }
          </Label>
        </RadioItem>
        <RadioItem value="no">
          <Label>
            { t('field.specialRulesDeclaration.options.no') }
          </Label>
        </RadioItem>
      </Radios>
      <Button>
        { t('form.buttons.continue.label', { ns: 'common' }) }
      </Button>
    </>
  );
}
