import { useTranslation } from "../../i18n";

export default async function Head({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['start', 'common']);
  return (
    <>
      <title>{`${ t('start:makeAClaim') } - ${ t('common:serviceName') } - ${ t('common:govuk') }`}</title>
    </>
  );
}
