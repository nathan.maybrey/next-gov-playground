import {
  Button,
  InsetText,
  BackLink,
  Typography
} from 'govuk-ui';
import { useTranslation } from '../../i18n';

export default async function Index({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['start', 'common']);
  return (
    <>
      <BackLink href='/verification-code'/>
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('makeAClaim') }
      </Typography>
      <Typography>
        { t('toMakeAnApplication') }
      </Typography>
      <InsetText>
        { t('forSomeoneElse') }
        <br/><br/>
        { t('theContactDetails') }
      </InsetText>
      <Typography variant="m">
        { t('otherWays') }
      </Typography>
      <Typography>
        { t('ifYouCannotApplyOnline') }, <a
        href="https://www.gov.uk/government/publications/attendance-allowance-claim-form"
        className="govuk-link">{ t('youCanApplyByPost') }</a>.
        { t('theAddressToSend') }
      </Typography>
      <Typography variant="m">
        { t('ifYouMightHaveSixMonths') }
      </Typography>
      <Typography>
        { t('thereAreSpecialRules') }
      </Typography>
      <Typography>
        { t('youNeedToApply') } <a className="govuk-link" href="https://www.gov.uk/government/publications/attendance-allowance-claim-form"> { t('usingThePaper') }</a>.
      </Typography>
      <Typography>
        { t('ifYouAreNotThePersonApplying') }
      </Typography>
      <input hidden id='start' name='start' defaultValue={1} />
      <Button isStartButton={true}>
        { t('form.buttons.applyNow', { ns: 'common' }) }
      </Button>
      <Typography variant="m">
        { t('ifYouDisagree') }
      </Typography>
      <Typography>
        { t('youCan') } <a
        href="https://www.gov.uk/mandatory-reconsideration"
        className="govuk-link">
          { t('challengeADecision') }
        </a> { t('mandatoryReconsideration') }
      </Typography>
      <Typography variant="m">
        { t('complaints', { ns: 'common' }) }
      </Typography>
      <Typography>
        { t('youCan') } <a
          href="https://www.gov.uk/government/organisations/department-for-work-pensions/about/complaints-procedure#who-to-contact-1"
          className="govuk-link">
          { t('complainToDWP') }
        </a> { t('ifYouAreUnhappy') }
      </Typography>
    </>
  );
}
