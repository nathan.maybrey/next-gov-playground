import {
  Button,
  BackLink,
  Textarea,
  Hint,
  Typography
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('treatment-surgery-information');
  const { t } = await useTranslation(lng, ['treatment-surgery-information', 'common']);
  return (
    <>
      <BackLink href='/renal-dialysis-help' />
      { validation && renderErrorSummary(validation) }
      <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
        { t('pageTitle') }
      </Typography>
      <Typography>
        { t('field.treatmentSurgeryInformation.canYouTellUs') }
      </Typography>
      <Hint>
      { t('field.treatmentSurgeryInformation.hint') }
      </Hint>
      <Textarea
        name="treatment-surgery-information"
        rows={8}
        value={data?.["treatment-surgery-information"]}
        errorMessage={validation?.["treatment-surgery-information"]?.inline}
      />
      <Button>
        { t('form.buttons.continue.label', { ns: 'common' }) }
      </Button>
    </>
  );
}
