import { useTranslation } from "../../i18n";

export default async function Head({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['treatment-surgery', 'common']);
  return (
    <>
      <title>{`${ t('treatment-surgery:pageTitle') } - ${ t('common:serviceName') } - ${ t('common:govuk') }`}</title>
    </>
  );
}
