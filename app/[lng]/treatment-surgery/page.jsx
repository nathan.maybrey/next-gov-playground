import {
  Button,
  BackLink,
  Radios,
  RadioItem,
  Label,
  Typography, Legend
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import getPageData from "../../helpers/get-page-data";
import renderErrorSummary from '../../helpers/error-summary';

export default async function Index({ params: { lng } }) {
  const { t } = await useTranslation(lng, ['treatment-surgery', 'common']);
  const { data, validation } = await getPageData('treatment-surgery');
  return (
    <>
      <BackLink href='/renal-dialysis-help' />
      { validation && renderErrorSummary(validation) }
      <Radios name="treatment-surgery" value={data?.['treatment-surgery']} errorMessage={validation?.['treatment-surgery']?.inline}>
        <Legend>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('pageTitle') }
          </Typography>
        </Legend>
        <RadioItem value="yes">
          <Label>
            { t('field.treatmentSurgery.options.yes') }
          </Label>
        </RadioItem>
        <RadioItem value="no">
          <Label>
            { t('field.treatmentSurgery.options.no') }
          </Label>
        </RadioItem>
      </Radios>
      <Button>
        { t('form.buttons.continue.label', { ns: 'common' }) }
      </Button>
    </>
  );
}
