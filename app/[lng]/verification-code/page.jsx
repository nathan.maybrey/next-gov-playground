import {
  Button,
  Input, Label,
  Typography
} from 'govuk-ui';
import { useTranslation } from '../../i18n';
import renderErrorSummary from '../../helpers/error-summary';
import getPageData from "../../helpers/get-page-data";

export default async function Index({ params: { lng } }) {
  const { data, validation } = await getPageData('verification-code');
  const { t } = await useTranslation(lng, ['verification-code', 'common']);
  return (
    <>
      { validation && renderErrorSummary(validation) }
      <Input classes="govuk-input--width-10" name="verificationCode" id="verificationCode" value={data?.verificationCode} errorMessage={validation?.verificationCode?.inline}>
        <Label>
          <Typography variant="l" component="h1" classes="govuk-!-margin-bottom-3">
            { t('pageTitle') }
          </Typography>
        </Label>
      </Input>
      <Button>
        { t('common:form.buttons.continue.label') }
      </Button>
    </>
  );
}
