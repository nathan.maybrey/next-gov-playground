import { ErrorSummary, ErrorSummaryMessage } from "govuk-ui";

export default function renderErrorSummary(validation) {
  return (
    <ErrorSummary>
      { Object.keys(validation).map((key) => (
        <ErrorSummaryMessage id={key} key={key}>{validation[key].summary}</ErrorSummaryMessage>
      ))}
    </ErrorSummary>
  );
}
