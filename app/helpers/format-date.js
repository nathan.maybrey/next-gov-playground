import { DateTime } from 'luxon';

export default function formatDate(dateString) {
  const date = DateTime.fromFormat(dateString, "d-M-yyyy");
  return `${date.day} ${date.monthShort} ${date.year}`;
}
