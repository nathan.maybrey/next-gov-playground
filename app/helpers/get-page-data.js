import { cookies } from "next/headers";
import {getDataForPage, getValidationErrors, getValidationErrorsForPage} from "../../lib/session";

export default async function getPageData(pageId) {
  const nextCookies = cookies();
  const sessionId = nextCookies.get(process.env.SESSION_COOKIE_NAME)?.value;
  const data = await getDataForPage(sessionId, pageId);
  const validation = await getValidationErrorsForPage(sessionId, pageId);
  return { data, validation };
}

export async function getValidationErrorsForCurrentSession() {
  const nextCookies = cookies();
  const sessionId = nextCookies.get(process.env.SESSION_COOKIE_NAME)?.value;
  return await getValidationErrors(sessionId);
}
