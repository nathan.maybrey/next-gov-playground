import Script from 'next/script'
import { Container, Grid, Footer, MainWrapper } from 'govuk-ui';
import Header from "./Header";

import '../styles/govuk.sass'
import '../styles/custom.scss'

export default async function RootLayout({children}) {
  return (
    <html lang="en">
      <body className="govuk-template__body govuk-background-grey">
        <Script id="gov-js-enabled">
          {`document.body.className = ((document.body.className) ? document.body.className + ' js-enabled' : 'js-enabled')`}
        </Script>
        <div className="govuk-background-white">
          <Header title={"Apply for Attendance Allowance"}/>
          <Container width={true}>
            <main>
              <Grid variant="row">
                <Grid variant="two-thirds">
                  <MainWrapper>
                    <form action="/api/form-handler" method="post">
                      {children}
                    </form>
                  </MainWrapper>
                </Grid>
              </Grid>
            </main>
          </Container>
          <Script src="/js/govuk.js" strategy="beforeInteractive"/>
          <Script id="govuk-init">
            {`window?.GOVUKFrontend?.initAll()`}
          </Script>
        </div>
        <Footer />
      </body>
    </html>
  );
}
