import got from 'got';

export const getAddressesByPostcode = async (postcode) => {
  if (/[^a-z0-9 ]/i.test(String(postcode))) {
    throw new SyntaxError('Postcode contains invalid characters');
  }

  console.log(`Requesting GET /lookup/address?postcode=${postcode}`);
  const result = await got(`${process.env.ADDRESS_LOOKUP_API_ENDPOINT}/lookup/address`, {
    method: 'GET',
    searchParams: { postcode },
    responseType: 'json',
    https: {
      rejectUnauthorized: false, // TODO: for production readiness CA_CHAIN will need to be consumed and this value should be set by env variable
    }
  });

  return result?.body?.data;
}
