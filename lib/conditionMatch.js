import match from './match';
import { arraysAreEqual, arrayContains } from './array';

export default function conditionMatch(routingConfig, data) {
  console.log('Conditional match check ...')
  let routingMatch = false;
  let matchedLink;
  routingConfig.forEach((routingCondition) => {
    if (routingMatch) return;
    if (routingCondition.page) {
      // no condition at all
      if (!routingCondition.condition) {
        console.log('No condition specified');
        routingMatch = true;
        matchedLink = routingCondition.page;
        console.log(`Condition matched by default`);
        console.log(`Next page is '${routingCondition.page}'`);
      // exact value
      } else if (routingCondition.condition?.match === match.value) {
        if (routingCondition.condition.field && routingCondition.condition.value) {
          const value = data[`${routingCondition.condition.field}`];
          console.log(`Value match specified: '${routingCondition.condition.field}' must equal '${routingCondition.condition.value}'`);
          console.log(`Data stored for '${routingCondition.condition.value}' is '${value}'`);
          if (data[`${routingCondition.condition.field}`] === routingCondition.condition.value) {
            routingMatch = true;
            console.log(`Condition matched!`);
            matchedLink = routingCondition.page;
          } else {
            console.log(`Condition not matched, next condition ...`);
          }
        } else {
          console.log(`Error: routing condition does not have a field and a value`);
        }
      // greater than a provided value
      } else if (routingCondition.condition?.match === match.greaterThan) {
        if (routingCondition.condition.field && routingCondition.condition.value) {
          const value = data[`${routingCondition.condition.field}`];
          console.log(`Greater than match specified: '${routingCondition.condition.field}' must be greater than '${routingCondition.condition.value}'`);
          console.log(`Data stored for '${routingCondition.condition.value}' is '${value}'`);
          if (data[`${routingCondition.condition.field}`] > routingCondition.condition.value) {
            routingMatch = true;
            console.log(`Condition matched!`);
            matchedLink = routingCondition.page;
          } else {
            console.log(`Condition not matched, next condition ...`);
          }
        } else {
          console.log(`Error: routing condition does not have a field and a value`);
        }
      // less than a provided value
      } else if (routingCondition.condition?.match === match.lessThan) {
        if (routingCondition.condition.field && routingCondition.condition.value) {
          const value = data[`${routingCondition.condition.field}`];
          console.log(`Less than match specified: '${routingCondition.condition.field}' must be less than '${routingCondition.condition.value}'`);
          console.log(`Data stored for '${routingCondition.condition.value}' is '${value}'`);
          if (data[`${routingCondition.condition.field}`] < routingCondition.condition.value) {
            routingMatch = true;
            console.log(`Condition matched`);
            matchedLink = routingCondition.page;
          } else {
            console.log(`Condition not matched, next condition ...`);
          }
        } else {
          console.log(`Error: routing condition does not have a field and a value`);
        }
      // any one of the provided values
      } else if (routingCondition.condition?.match === match.anyOne) {
        if (routingCondition.condition.field && routingCondition.condition.value
          && Array.isArray(routingCondition.condition.value)) {
          if (Array.isArray(data[`${routingCondition.condition.field}`])) {
            const value = data[`${routingCondition.condition.field}`];
            console.log(`Any one in array match specified: '${routingCondition.condition.field}' must contain '${routingCondition.condition.value}'`);
            console.log(`Data stored for '${routingCondition.condition.value}' is '${value}'`);
            if (data[`${routingCondition.condition.field}`]?.some((e) => routingCondition.condition.value.includes(e))) {
              routingMatch = true;
              console.log(`Condition matched`);
              matchedLink = routingCondition.page;
            } else {
              console.log(`Condition not matched, next condition ...`);
            }
          }
        } else {
          console.log(`Error: routing condition does not have a field and a value`);
        }
      // all of the provided values
      } else if (routingCondition.condition?.match === match.all) {
        if (routingCondition.condition.field && routingCondition.condition.value
          && Array.isArray(routingCondition.condition.value)) {
          if (Array.isArray(data[`${routingCondition.condition.field}`])) {
            const value = data[`${routingCondition.condition.field}`];
            console.log(`Equal array match specified: '${routingCondition.condition.field}' must equal '${routingCondition.condition.value}'`);
            console.log(`Data stored for '${routingCondition.condition.value}' is '${value}'`);
            if (arraysAreEqual(routingCondition.condition.value, data[`${routingCondition.condition.field}`])) {
              routingMatch = true;
              console.log(`Condition matched`);
              matchedLink = routingCondition.page;
            } else {
              console.log(`Condition not matched, next condition ...`);
            }
          }
        } else {
          console.log(`Error: routing condition does not have a field and a value`);
        }
      // none of the provided values
      } else if (routingCondition.condition?.match === match.none) {
        console.log('None in array match specified');
        if (routingCondition.condition.field && routingCondition.condition.value
          && Array.isArray(routingCondition.condition.value)) {
          if (Array.isArray(data[`${routingCondition.condition.field}`])) {
            const value = data[`${routingCondition.condition.field}`];
            console.log(`Not in array match specified: '${routingCondition.condition.field}' must not contain '${routingCondition.condition.value}'`);
            console.log(`Data stored for '${routingCondition.condition.value}' is '${value}'`);
            if (!arrayContains(routingCondition.condition.value, data[`${routingCondition.condition.field}`])) {
              routingMatch = true;
              console.log(`Condition matched`);
              matchedLink = routingCondition.page;
            } else {
              console.log(`Condition not matched, next condition ...`);
            }
          }
        } else {
          console.log(`Error: routing condition does not have a field and a value`);
        }
      }
    }
  });
  return matchedLink;
};
