import type { NextRequest } from 'next/server';
import { NextResponse } from 'next/server';
import acceptLanguage from 'accept-language';
import { fallbackLng, languages } from './app/i18n/settings';
import { v4 } from "uuid";

acceptLanguage.languages(languages)

export const config = {
  matcher: '/:lng*'
}

const i18nCookieName = 'i18next'

export function middleware(req: NextRequest) {
  let lng
  if (req.cookies.has(i18nCookieName)) lng = acceptLanguage.get(req.cookies.get(i18nCookieName).value)
  if (!lng) lng = acceptLanguage.get(req.headers.get('Accept-Language'))
  if (!lng) lng = fallbackLng

  // Redirect if lng in path is not supported
  if (
    !languages.some(loc => req.nextUrl.pathname.startsWith(`/${loc}`)) &&
    !req.nextUrl.pathname.startsWith('/_next') &&
    !req.nextUrl.pathname.startsWith('/assets') &&
    !req.nextUrl.pathname.startsWith('/css') &&
    !req.nextUrl.pathname.startsWith('/js') &&
    !req.nextUrl.pathname.startsWith('/api')
  ) {
    return NextResponse.redirect(new URL(`/${lng}${req.nextUrl.pathname}`, req.url))
  }

  const sessionCookie = req.cookies.has(process.env.SESSION_COOKIE_NAME);
  const response = NextResponse.next();
  if (!sessionCookie) {
    console.log("No session cookie present, generating one");
    response.cookies.set(process.env.SESSION_COOKIE_NAME, v4())
    console.log("Session storage set to: ", process.env.SESSION_STORAGE);
  }

  if (req.headers.has('referer')) {
    const refererUrl = new URL(req.headers.get('referer'))
    const lngInReferer = languages.find((l) => refererUrl.pathname.startsWith(`/${l}`))
    if (lngInReferer) response.cookies.set(i18nCookieName, lngInReferer)
  }

  return response
}
