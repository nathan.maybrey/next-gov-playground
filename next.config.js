const nextConfig = {
  reactStrictMode: true,
  experimental: {
    appDir: true
  },
  output: 'standalone', // Needed for docker
  env: {
    SESSION_COOKIE_NAME: process.env.SESSION_COOKIE_NAME || 'session_cookie',
    REDIS_HOST: process.env.REDIS_HOST || 'localhost',
    REDIS_PORT: process.env.REDIS_PORT || 6380,
    ROOT_PAGE: 'verification-code',
    ADDRESS_LOOKUP_API_ENDPOINT: process.env.ADDRESS_LOOKUP_API_ENDPOINT || 'https://shared-dev-blue-app-ecs-lb.dev.rbcdwp.internal/address-service/api/v2',
    SESSION_STORAGE: process.env.SESSION_STORAGE || 'file',
  },
  async redirects() {
    return [
      {
        source: '/',
        destination: '/verification-code',
        permanent: true
      }
    ]
  }
}

module.exports = nextConfig
