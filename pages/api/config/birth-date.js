import urls from '../../../lib/urls';
import match from '../../../lib/match';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.personClaiming,
  next: [
    {
      page: urls.birthDateIneligible,
      condition: {
        field: 'birth-date-year',
        value: 1955,
        match: match.greaterThan,
      },
    },
    {
      page: urls.otherBenefits,
    },
  ],
  validation: {
    'birth-date-day': string()
      .required(validationError('Date of birth must contain a day.'))
  }
}
