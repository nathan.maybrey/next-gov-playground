import urls from '../../../lib/urls';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.homeAddressSelect,
  next: urls.illnessDisability,
  validation: {
    'care-home-hospital': string()
      .required(validationError('You must select an option.'))
  }
};
