import { string } from 'yup';
import urls from '../../../lib/urls';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.nationalInsurance,
  next: urls.homeAddressPostcode,
  validation: {
    'full-name': string()
      .required(validationError('Enter your full name.'))
      .max(500, validationError('Full name must be 500 characters or less.')),
  }
}
