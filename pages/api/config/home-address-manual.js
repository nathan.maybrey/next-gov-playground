import urls from '../../../lib/urls';

module.exports = {
  previous: urls.homeAddressPostcode,
  next: urls.careHomeHospital,
}