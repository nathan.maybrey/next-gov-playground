import urls from '../../../lib/urls';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.fullName,
  next: urls.homeAddressSelect,
  validation: {
    postcode: string()
      .required(validationError('Enter a postcode.'))
      .matches(/^([A-Za-z][A-Ha-hK-Yk-y]?[0-9][A-Za-z0-9]? ?[0-9][A-Za-z]{2}|[Gg][Ii][Rr] ?0[Aa]{2})$/, () => validationError('Enter a real postcode.')),
  }
}
