import urls from '../../../lib/urls';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.homeAddressPostcode,
  next: urls.careHomeHospital,
  validation: {
    'home-address': string()
      .required(validationError('You must select an option.'))
      .matches(/^\d{1,12}$/, () => validationError('You must select an option.'))
  }
}
