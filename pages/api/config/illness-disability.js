import urls from '../../../lib/urls';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.careHomeHospital,
  next: urls.specialRulesDeclaration,
  validation: {
    'illness-disability': string()
      .required(validationError('You need to tell us if you have any illnesses, physical or mental disabilities.'))
      .max(5000, validationError('Your answer must be 5000 characters or less.')),
  }
}
