import urls from '../../../lib/urls';
import match from '../../../lib/match';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.otherBenefits,
  next: [
    {
      page: urls.nationalityOther,
      condition: {
        field: urls.nationality,
        value: 'other',
        match: match.value,
      },
    },
    {
      page: urls.fullName,
    },
  ],
  validation: {
    'national-insurance': string()
      .required(validationError('Enter a National Insurance number in the correct format.'))
      .matches(/^(?!BG|GB|NK|KN|TN|NT|ZZ)[ABCEGHJ-PRSTW-Z][ABCEGHJ-NPRSTW-Z]\d{6}[A-D]$/i, () => validationError('Enter a National Insurance number that is 2 letters, 6 numbers, then A, B, C or D, like QQ 12 34 56 C.'))
  }
}
