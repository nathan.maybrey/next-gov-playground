import urls from '../../../lib/urls';

module.exports = {
  previous: urls.nationality,
  next: urls.residenceCountry,
}
