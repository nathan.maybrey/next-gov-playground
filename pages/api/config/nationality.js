import urls from '../../../lib/urls';
import match from '../../../lib/match';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.otherBenefits,
  next: [
    {
      page: urls.nationalityOther,
      condition: {
        field: urls.nationality,
        value: 'other',
        match: match.value,
      },
    },
    {
      page: urls.residenceCountry,
    },
  ],
  validation: {
    nationality: string()
      .required(validationError('Select if you are British, Irish or a citizen of a different country.'))
  }
}
