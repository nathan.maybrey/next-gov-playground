import urls from '../../../lib/urls';
import match from '../../../lib/match';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.birthDate,
  next: [
    {
      page: urls.otherBenefitsIneligible,
      condition: {
        field: urls.otherBenefits,
        value: [
          'dla',
          'pip',
          'adp',
        ],
        match: match.anyOne,
      },
    },
    {
      page: urls.nationality,
    },
  ],
  validation: {
    'other-benefits': string()
      .required(validationError('You must complete this section - select one or more of the options.'))
  }
}
