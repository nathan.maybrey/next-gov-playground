import urls from '../../../lib/urls';
import match from '../../../lib/match';
import {string} from "yup";
import {validationError} from "../../../lib/validation-error";

module.exports = {
  previous: urls.start,
  next: [
    {
      page: urls.personClaimingIneligible,
      condition: {
        field: urls.personClaiming,
        value: 'official-capacity',
        match: match.value,
      },
    },
    {
      page: urls.birthDate,
    },
  ],
  validation: {
    'person-claiming': string()
      .required(validationError('You must select an option.'))
  }
}
