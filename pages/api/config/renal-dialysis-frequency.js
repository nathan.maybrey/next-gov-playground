import urls from '../../../lib/urls';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.renalDialysisWhen,
  next: urls.renalDialysisHelp,
  validation: {
    'renal-dialysis-frequency': string()
      .required(validationError('You must select one of the values.')),
  }
}
