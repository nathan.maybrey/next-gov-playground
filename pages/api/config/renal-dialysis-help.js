import urls from '../../../lib/urls';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.renalDialysisFrequency,
  next: urls.treatmentSurgery,
  validation: {
    'renal-dialysis-help': string()
      .required(validationError('You must enter either yes or no.')),
  }
}
