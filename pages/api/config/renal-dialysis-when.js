import urls from '../../../lib/urls';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.renalDialysisWhere,
  next: urls.renalDialysisFrequency,
  validation: {
    'renal-dialysis-when': string()
      .required(validationError('We need to know when you receive your treatment.')),
  }
}
