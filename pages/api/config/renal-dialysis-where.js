import urls from '../../../lib/urls';
import match from '../../../lib/match';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.renalDialysis,
  next: [
    {
      page: urls.renalDialysisWhen,
      condition: {
        field: urls.renalDialysisWhere,
        value: 'home',
        match: match.value,
      },
    },
    {
      page: urls.treatmentSurgery,
    },
  ],
  validation: {
    'renal-dialysis-where': string()
      .required(validationError('We need to know where you receive your treatment.')),
  }
}
