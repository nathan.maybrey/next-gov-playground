import urls from '../../../lib/urls';
import match from '../../../lib/match';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: [
    {
      page: urls.surgeryMedicalCentre,
      condition: {
        field: urls.specialRulesDeclaration,
        value: 'yes',
        match: match.value,
      },
    },
    {
      page: urls.specialRulesDeclaration,
    },
  ],
  next: [
    {
      page: urls.renalDialysisWhere,
      condition: {
        field: urls.renalDialysis,
        value: 'yes',
        match: match.value,
      },
    },
    {
      page: urls.treatmentSurgery,
    },
  ],
  validation: {
    'renal-dialysis': string()
      .required(validationError('You must enter either yes or no.')),
  }
}
