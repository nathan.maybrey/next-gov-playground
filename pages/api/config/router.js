import { getDataForPage } from '../../../lib/session';
import conditionMatch from '../../../lib/conditionMatch';

export default async function router(sessionId) {
  console.log('Router running ...');
  const rootPage = process.env.ROOT_PAGE;
  if (rootPage) {
    console.log(`Root page is '${rootPage}'`)
    return handleRouting(rootPage, sessionId);
  } else {
    console.log('No root page set, aborting');
    console.log('Routing complete');
  }
}

async function handleRouting(page, sessionId) {
  console.log(`Checking config for page '${page}'`);
  let config = '';
  try {
    config = require(`./${page}`);
  } catch (err) {
    console.log(err);
    console.log(`Error: no page config found for page ${page} or config is badly formatted`);
    return page;
  }

  let nextPage = config.next;
  if (Array.isArray(nextPage)) {
    console.log('Next page is an array of options ...');
    const data = await getDataForPage(sessionId, page);
    nextPage = conditionMatch(config.next, data);
  } else {
    console.log(`Next page is a single option '${nextPage}'`);
  }

  //does next page have an answer?
  console.log(`Checking answers for page '${nextPage}'`);
  if(! await getDataForPage(sessionId, nextPage)) {
    console.log(`No answers for page '${nextPage}'`);
    console.log(`Sending user to '${nextPage}'`);
    console.log('Routing complete')
    return nextPage;
  } else {
    console.log(`Page '${nextPage}' already answered`);
    return await handleRouting(nextPage, sessionId);
  }
}


