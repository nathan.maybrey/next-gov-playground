import urls from '../../../lib/urls';
import match from '../../../lib/match';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.illnessDisability,
  next: [
    {
      page: urls.personClaimingIneligible,
      condition: {
        field: urls.specialRulesDeclaration,
        value: 'yes',
        match: match.value,
      },
    },
    {
      page: urls.renalDialysis,
    },
  ],
  validation: {
    'special-rules-declaration': string()
      .required(validationError('You must enter either yes or no.'))
  }
}
