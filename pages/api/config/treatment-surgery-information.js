import urls from '../../../lib/urls';
import match from '../../../lib/match';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: urls.treatmentSurgery,
  next: [
    {
      page: urls.checkAnswersSpecialRules,
      condition: {
        field: urls.specialRulesDeclaration,
        value: 'yes',
        match: match.value,
      },
    },
    {
      page: urls.checkAnswersDetails,
    },
  ],
  validation: {
    'treatment-surgery-information': string()
      .required(validationError('We need to know what other treatments you receive or whether you are waiting for surgery.'))
      .max(5000, validationError('Your answer must be 5000 characters or less.')),
  }
}
