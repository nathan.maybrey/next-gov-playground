import urls from '../../../lib/urls';
import match from '../../../lib/match';
import { string } from 'yup';
import { validationError } from '../../../lib/validation-error';

module.exports = {
  previous: [
    {
      page: urls.renalDialysis,
      condition: {
        field: urls.renalDialysis,
        value: 'no',
        match: match.value,
      },
    },
    {
      page: urls.renalDialysisWhere,
      condition: {
        field: urls.renalDialysisWhere,
        value: 'hospital',
        match: match.value,
      },
    },
    {
      page: urls.renalDialysisHelp,
    },
  ],
  next: [
    {
      page: urls.treatmentSurgeryInformation,
      condition: {
        field: urls.treatmentSurgery,
        value: 'yes',
        match: match.value,
      },
    },
    {
      page: urls.checkAnswersSpecialRules,
      condition: {
        field: urls.specialRulesDeclaration,
        value: 'yes',
        match: match.value,
      },
    },
    {
      page: urls.checkAnswersDetails,
    },
  ],
  validation: {
    'treatment-surgery': string()
      .required(validationError('You must enter either yes or no.')),
  }
}
