import { string } from 'yup';
import urls from '../../../lib/urls';
import { validationError } from "../../../lib/validation-error";

module.exports = {
  next: urls.start,
  validation: {
    verificationCode: string()
      .required(validationError('You need to enter a verification code.'))
      .max(25, validationError('Your answer must be 25 characters or less.')),
  }
}
