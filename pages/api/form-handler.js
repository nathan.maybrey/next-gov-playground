import {clearValidationErrorsForPage, setDataForPage, setValidationErrorsForPage} from "../../lib/session";
import router from './config/router';
import validate from "./validate";

export default async function handler (req, res) {
  if (req.method === 'POST') {
    const referer = req.headers?.referer;
    // TODO: Think about user manipulating URL
    const pageSlug = referer.split('/').pop();

    try {
      const sessionId = req.cookies[process.env.SESSION_COOKIE_NAME];
      await setDataForPage(sessionId, pageSlug, {
        ...req.body
      });

      const validationErrors = await validate(sessionId, pageSlug);
      if (validationErrors) {
        await setValidationErrorsForPage(sessionId, pageSlug, validationErrors)
        return res.redirect(302, `/${pageSlug}`);
      }

      console.log(`No validation errors, clearing any existing validation state for page: ${pageSlug}`)
      await clearValidationErrorsForPage(sessionId, pageSlug);

      const nextPage = await router(sessionId);

      return res.redirect(302, `/${nextPage}`);

    } catch (e) {
      console.log("Error: ", e)
    }
  }
}
