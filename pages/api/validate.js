import { getDataForPage } from "../../lib/session";
import { object } from 'yup';

export default async function validate(sessionId, pageId) {
  const pageData = await getDataForPage(sessionId, pageId);

  // Get the page config to get the validation
  let config = '';
  try {
    config = require(`./config/${pageId}`);
  } catch (err) {
    console.log(err);
    console.log(`Error: no page config found for page ${pageId} or config is badly formatted`);
    return pageId;
  }

  if (!config.validation) {
    console.log(`No validation defined in config for page: ${pageId}, continuing without validation`)
    return null;
  }

  // Convert validation object structure to Yup schema ready to validate.
  const validationSchema = object(config.validation);

  try {
    console.log(`Validating data for page: ${pageId}`)
    await validationSchema.validate({ ...pageData, testField: '' }, { abortEarly: false });
  } catch (validationErrors) {
    console.log(`Validation errors found for page: ${pageId}`);
    return yupErrorToErrorObject(validationErrors);
  }
}

const yupErrorToErrorObject = (yupError) => {
  const errorObject = {};

  yupError.inner?.forEach((x) => {
    if (x.path !== undefined && !errorObject[x.path]) {
      errorObject[x.path] = x.errors[0];
    }
  });

  return errorObject;
}
